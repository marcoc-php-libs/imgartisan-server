<?php

use mrblue\mvc\Logging\GcpLogger;

if( getenv('GCP_ERROR_LOG_PROJECT') ){
    GcpLogger::setLogClient(new \Google\Cloud\Logging\LoggingClient([
        'projectId' => getenv('GCP_ERROR_LOG_PROJECT'),
        'transport' => 'rest'
    ]));
}

function logException(\Throwable $th) {
    if( getenv('GCP_ERROR_LOG_PROJECT') ){
        GcpLogger::exception($th);
    } else {
        file_put_contents('php://stderr', (string) $th);
    }
}

set_error_handler(function( ...$args ){
    logException(GcpLogger::errorExceptionGen( ...$args ));
    return true;
});

function processException(\Throwable $th) {
    $error_code = $th->getCode() ? : 500;


    if( $th instanceof UserException ){
        if( $error_code < 1000 ){
            http_response_code( $error_code );
        } else {
            http_response_code( intval($error_code/10) );
        }
        $user_message = $th->getMessage() ? : 'Generic Error';
    } else {
        http_response_code( 500 );
        $user_message = 'Generic Error';
        logException($th);
    }
    if( ! headers_sent() ){
        header('Content-Type: application/json');
    }

    return json_encode([
        'status' => 'error',
        'code' => $error_code,
        'message' => $user_message
    ]);
}