<?php

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

/**
 * @var \Psr\Http\Message\ServerRequestInterface $request
 */

CONST ALLOWED_RESOURCE_UPLOAD_METHODS = ['PUT','POST'];

function mapImageKitException( \Throwable $th ){
    if ( $th instanceof \InvalidArgumentException ) {
        return new UserException('Invalid Argument: '.$th->getMessage(),400);
    } elseif ( $th instanceof \DomainException ) {
        return new UserException('Domain Error: '.$th->getMessage(),400);
    } elseif ( $th instanceof \OutOfRangeException ) {
        throw new UserException('Out of Range: '.$th->getMessage(),400);
    } else {
        return $th;
    }
}

$instructions = json_decode($_POST['instructions']??'{}',true);

if( empty($instructions) || ! is_array($instructions) ){
    throw new UserException('instructions must be a not empty array',400);
}

$jobs = [];

foreach( $instructions as $instruction ){

    $dowload_file = empty($instruction['resource_url']);

    if( $dowload_file ){
        if( sizeof($instructions) > 1 ){
            throw new UserException('Download mode can be used only when there is a single instruction',400);
        }
    } else {
        if( is_string($instruction['resource_url']) && substr($instruction['resource_url'],0,5) == 'gs://' ){
            $exploded = explode("/",$instruction['resource_url'],4);
            if( sizeof($exploded) != 4 ){
                throw new UserException('resource_url for Google Storage must have format: "gs://bucket_name/object_path"',400);
            }
            list(,,$bucket,$object) = $exploded;
            $ResourceUpload = (object) [
                'type' => 'gs',
                'bucket' => $bucket,
                'object' => $object
            ];

            if( ! isset($GsClient) ){
                $GsClient = new \Google\Cloud\Storage\StorageClient();
            }
        } else {
            $ResourceUpload = (object) [
                'type' => 'http',
                'url' => filter_var($instruction['resource_url'], FILTER_VALIDATE_URL, [
                    'flags' => FILTER_FLAG_SCHEME_REQUIRED|FILTER_FLAG_HOST_REQUIRED
                ]),
                'method' => $instruction['resource_method'] ?? 'PUT'
            ];
            if( ! $ResourceUpload->url ){
                throw new UserException('resource_url is not a valid url',400);
            }
            if( ! in_array($ResourceUpload->method,ALLOWED_RESOURCE_UPLOAD_METHODS) ){
                throw new UserException('resource_method must be '.implode(' or ',ALLOWED_RESOURCE_UPLOAD_METHODS),400);
            }

            if( ! isset($HttpClient) ){
                $HttpClient = new \GuzzleHttp\Client([
                    'timeout' => 30
                ]);
            }
        }
        
    }

    if( empty($instruction['job']) || ! is_array($instruction['job']) ){
        throw new UserException('job must be a non empty array',400);
    }

    $files_map = array_map(function($entry){
        if( isset($entry['error']) && $entry['error'] !== UPLOAD_ERR_OK ){
            throw new UserException('upload file error',400);
        }
        if( empty($entry['tmp_name']) || ! is_uploaded_file($entry['tmp_name']) ){
            throw new UserException('file not uploaded',400);
        }
        return $entry['tmp_name'];
    },$_FILES);

    if( empty($files_map['main']) ){
        throw new UserException('main file missing',400);
    }

    try {
        $jobs[] = [
            'ImageKit' => new \mrblue\imgartisan\ImageKit($instruction['job'],$files_map),
            'ResourceUpload' => $ResourceUpload ?? false
        ];
    } catch (\Throwable $th) {
        throw mapImageKitException($th);
    }

}



$promises = [];
foreach( $jobs as $job ){
    try {
        $job['ImageKit']->process();
    } catch (\Throwable $th) {
        throw mapImageKitException($th);
    }
    $format = strtolower( $job['ImageKit']->getImageFormat() );
    $blob = $job['ImageKit']->toBlob();
    unset($job['ImageKit']);
    $filesize = strlen($blob);
    
    if( $job['ResourceUpload'] ){
        $ResourceUpload = $job['ResourceUpload'];
        if( $ResourceUpload->type == 'gs' ){
            $promises[] = $Promise = $GsClient->bucket($ResourceUpload->bucket)->uploadAsync($blob,[
                'name' => str_replace('{{format}}',$format,$ResourceUpload->object),
                'metadata' => [
                    'contentType' => 'image/'.$format
                ]
            ]);
        } else {
            $promises[] = $Promise = $HttpClient->requestAsync($ResourceUpload->method , $ResourceUpload->url,[
                'headers' => [
                    'Content-Type' => 'image/'.$format
                ],
                'body' => $blob
            ]);
        }
        
        $Promise->otherwise(
            function (\Throwable $th) use ($job) {
                throw new UserException((string) $th,502);
            }
        );
    } else {
        header('Content-Type: image/'.$format);
        header('Content-Disposition: attachment; filename="image.'.$format.'"');
        header('Content-Length: '.$filesize);
        return $blob;
    }
}

GuzzleHttp\Promise\Utils::unwrap($promises);

http_response_code(201);
header('Content-Type: application/json');
return json_encode([
    'status' => 'ok'
]);