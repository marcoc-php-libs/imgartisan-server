<?php

/**
 * index.php for docker implementation
 */

chdir( dirname(__DIR__) );

include 'vendor/autoload.php';

include 'src/init_environment.php';

try {
   $body = include 'src/run.php';
} catch (\Throwable $th) {
    $body = processException($th);
}

echo $body;

