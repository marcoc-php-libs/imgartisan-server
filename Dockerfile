FROM php:8.0-apache AS development

RUN apt-get update && \
	apt-get install -y \
		curl wget procps zip git g++ pkg-config \
		libssl-dev libpq-dev libicu-dev libmagickwand-dev

RUN pecl install imagick-3.5.1

#RUN docker-php-ext-enable imagick

COPY ./etc/apache /etc/apache2/sites-available/000-default.conf
	
RUN sed -i 's/80/${PORT}/g' /etc/apache2/ports.conf && \
	mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini" && \
	a2enmod rewrite

ARG user_developer_uid=1000
RUN useradd -u $user_developer_uid -g www-data -m -s /bin/bash developer

ENV PORT=80
ENV DISPLAY_ERRORS=1

FROM development AS production

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

COPY . /var/www/html

WORKDIR /var/www/html

RUN sh etc/composer-installer.sh
RUN --mount=type=secret,id=composer_auth,dst=/var/www/html/auth.json php -c . composer.phar install -o --no-dev

RUN chown www-data.www-data -R /var/www

ENV MAX_FILE_UPLOADS=20

