<?php

/**
 * index.php for cloud function implementation
 */

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Psr7\Response;

function run(ServerRequestInterface $request): ResponseInterface {

    include 'src/init_environment.php';

    try {
        $body = include 'src/run.php';
    } catch (\Throwable $th) {
        $body = processException($th);
    }

    $headers = [];
    foreach( headers_list() as $header ){
        list($k , $v) = explode(':',$header,2);
        $k = trim($k);
        $v = trim($v);
        header_remove($k);
        $headers[$k] = $v;
    }

    return new Response(http_response_code(),$headers,$body);
}